##This is the [ramses](https://bitbucket.org/rteyssie/ramses) bitbucket repository.
##You can also go to the code web site [here](http://www.ics.uzh.ch/~teyssier/ramses/RAMSES.html). 
Ramses is an open source code to model astrophysical systems, featuring self-gravitating, magnetised, compressible,
radiative fluid flows. It is based  on the Adaptive Mesh Refinement (AMR)  technique on a  fully-threaded graded octree. 
[ramses](https://bitbucket.org/rteyssie/ramses) is written in  Fortran 90 and is making intensive use of the Message 
Passing Interface (MPI) library.
Download the code by cloning the git repository using 
```
$ git clone https://bitbucket.org/rteyssie/ramses
```
Please register also to the [mailing list](http://groups.google.com/group/ramses_users).